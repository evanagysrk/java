package edu.ubb.biobolt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BioBoltApplication {

    public static void main(String args[]) {

        SpringApplication.run(BioBoltApplication.class, args);
    }
}
