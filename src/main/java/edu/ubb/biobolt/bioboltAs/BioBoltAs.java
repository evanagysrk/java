package edu.ubb.biobolt.bioboltAs;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.stereotype.Component;

import edu.ubb.biobolt.bioboltDTO.BioBoltDTO;
import edu.ubb.biobolt.model.BioBolt;

@Component
public class BioBoltAs {

    public BioBoltDTO modelToDto(BioBolt bioBolt) {
        BioBoltDTO bioBoltDTO = new BioBoltDTO();
        bioBoltDTO.setId(bioBolt.getId());
        bioBoltDTO.setTitle(bioBolt.getTitle());
        bioBoltDTO.setUser(bioBolt.getUser());
        bioBoltDTO.setContent(bioBolt.getContent());
        bioBoltDTO.setDate(bioBolt.getDate());

        return bioBoltDTO;
    }

    public BioBolt dtoToModel(BioBoltDTO bioBoltDto) {
        BioBolt bioBolt = new BioBolt();
        bioBolt.setId(bioBoltDto.getId());
        bioBolt.setTitle(bioBoltDto.getTitle());
        bioBolt.setUser(bioBoltDto.getUser());
        bioBolt.setContent(bioBoltDto.getContent());
        bioBolt.setDate(bioBoltDto.getDate());

        return bioBolt;
    }

    public Iterable<BioBoltDTO> modelToDto(Iterable<BioBolt> bioBoltok) {
        return StreamSupport.stream(bioBoltok.spliterator(), false).map( bioBolt -> modelToDto(bioBolt)).collect(Collectors.toList());
    }
}
