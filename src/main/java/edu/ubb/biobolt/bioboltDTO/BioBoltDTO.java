package edu.ubb.biobolt.bioboltDTO;

import java.io.Serializable;
import java.util.Date;

public class BioBoltDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String title;
    private String content;
    private String user;
    private Date date;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
