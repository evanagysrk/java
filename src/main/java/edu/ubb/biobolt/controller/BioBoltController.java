package edu.ubb.biobolt.controller;

import java.net.URI;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.print.PrintException;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import edu.ubb.biobolt.bioboltAs.BioBoltAs;
import edu.ubb.biobolt.bioboltDTO.BioBoltDTO;
import edu.ubb.biobolt.model.BioBolt;
import edu.ubb.biobolt.service.BioBoltService;

@RestController
@RequestMapping("/api/BioBoltTermekek/")
public class BioBoltController {

    private static final Logger LOGGER = Logger.getLogger(BioBoltController.class);

    @Autowired
    private BioBoltService bioBoltService;

    @Autowired
    private BioBoltAs bioBoltAs;

    @PostConstruct
    public void postConstructDemo() {
        BioBolt post1 = new BioBolt("Zakuszka", "2019", "Username", new Date());
        BioBolt post2 = new BioBolt("Szilva lekvar", "2019", "Username2", new Date());
	BioBolt post3 = new BioBolt("Eper lekvar", "2018", "Username", new Date());
        BioBolt post4 = new BioBolt("Tarkony", "2019", "Username2", new Date());

        bioBoltService.create(post1);
        bioBoltService.create(post2);
	bioBoltService.create(post3);
        bioBoltService.create(post4);
        bioBoltService.findAll().forEach(post -> LOGGER.info("" + post));
        bioBoltService.findByUser(post2.getUser()).forEach(post -> LOGGER.info("" + post));
    }

    @GetMapping
    public Iterable<BioBoltDTO> findAll() {
        return bioBoltAs.modelToDto(this.bioBoltService.findAll());
    }

    @GetMapping("/{id}")
    public BioBoltDTO findById(@PathVariable("id") Long id) throws PrintException {
        BioBolt bioBolt = this.bioBoltService.findById(id);
        if (bioBolt == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "BioBolt not found :(");
        }
        return bioBoltAs.modelToDto(bioBolt);
    }

    @PostMapping
    public ResponseEntity<BioBoltDTO> insert(@RequestBody BioBoltDTO bioBoltDto) {
        BioBolt bioBolt = this.bioBoltService.create(bioBoltAs.dtoToModel(bioBoltDto));
        return ResponseEntity.created(URI.create("/api/BioBoltTermekek/" + bioBolt.getId()))
                .body(bioBoltAs.modelToDto(bioBolt));
    }
}
