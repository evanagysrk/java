package edu.ubb.biobolt.model;

import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name = "Posts")
public class BioBolt extends BaseEntity {

    private static final long serialVersionUID = 1L;
    @Column(name = "Title")
    private String title;
    private String content;
    @Column(nullable = false)
    private String user;
    private Date date;

    public BioBolt() {

    }

    public BioBolt(String title, String content, String user, Date date) {
        this.title = title;
        this.content = content;
        this.user = user;
        this.date = date;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @PrePersist
    public void prePersist() {
        if (this.date == null) {
            this.date = new Date();
        }
    }

    @Override
    public String toString() {
        return "BioBolt{" + "id=" + id + ", title='" + title + '\'' + ", content='" + content + '\'' + ", user='"
                + user + '\'' + ", date=" + date + '}';
    }
}
