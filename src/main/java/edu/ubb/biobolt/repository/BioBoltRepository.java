package edu.ubb.biobolt.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import edu.ubb.biobolt.model.BioBolt;

@Repository
public interface BioBoltRepository extends CrudRepository<BioBolt, Long> {

    public List<BioBolt> findByUser(String user);
}
