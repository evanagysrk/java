package edu.ubb.biobolt.service;

import java.util.List;
import java.util.Optional;

import javax.persistence.PersistenceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ParseException;
import org.springframework.stereotype.Service;

import edu.ubb.biobolt.model.BioBolt;
import edu.ubb.biobolt.repository.BioBoltRepository;

@Service
public class BioBoltService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BioBoltService.class);

    @Autowired
    private BioBoltRepository bioBoltRepository;

    public BioBolt create(BioBolt bioBolt) {
        if (bioBolt == null) {
            LOGGER.error("BioBolt is null");
            throw new ServiceException("BioBolt is null");
        }
        LOGGER.info("BioBolt almost created");
        return bioBoltRepository.save(bioBolt);
    }

    public Iterable<BioBolt> findAll() {
        try {
            return bioBoltRepository.findAll();
        } catch (ParseException pe) {
            LOGGER.error("Error during findAll", pe);
            throw new ServiceException("Error during findAll", pe);
        }
    }

    public List<BioBolt> findByUser(String user) {
        try {
            return bioBoltRepository.findByUser(user);
        } catch (ParseException pe) {
            LOGGER.error("Error during findByUser", pe);
            throw new ServiceException("Error during findByUser", pe);
        }
    }

    public BioBolt findById(Long id) {
        try {
            Optional<BioBolt> bioBolt = bioBoltRepository.findById(id);
            if (bioBolt.isPresent()) {
                return bioBolt.get();
            }
            return null;
        } catch (PersistenceException pe) {
            LOGGER.error("Error during findById", pe);
            throw new ServiceException("Error during findById", pe);
        }
    }
}
